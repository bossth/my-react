import React,{ Component } from "react";

class Header extends Component {
    // state เป็นการเก็บค่าที่สามารถเปลี่ยนแปลงได้
    constructor(props) {
        super(props);
        this.state = {date : new Date()};
    }

    // Lifecycle วงจรชีวิตของ component
    // componentDidMount คือการติดข้อมูล กับ constructor อยู่สถานะเดียวกัน
    componentDidMount() {
        this.timerID = setInterval(() => this.tick(), 1000);
    }

    componentDidUpdate(){

    }

    // เอา component ออกจากตัวหน้าเว็บ
    componentWillUnmount(){
        clearInterval(this.timerID);
    }

    tick(){
        this.setState({date: new Date()});
    }

    render() {
        

        return (
            <div className="container-fluid">
                <div className="row bg-dark">
                    <div className="col-md-8 text-light">
                    <h2 className="text-left mt-2"><img style={{ height : 50 }} src="favicon.ico" alt="logo" /> บอสคุง</h2>
                    </div>
                    <div className="col-md-4 text-light">
                    <h5 className="text-right mt-3">{this.state.date.toLocaleTimeString()}</h5>
                    </div>
                </div> 
            </div>
        )
    }
}

export default Header;