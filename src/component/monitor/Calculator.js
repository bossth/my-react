import React, {Component} from 'react';
class Calculator extends Component {
    render(){
        return(
            <div>
                <h1 className="text-right">340.0</h1>
                <hr />
                <ul className="list-unstyled">
                    <li className="text-right text-success title">
                    บิงซู x 1 = 200
                    <button className="btn btn-light btn-sm">X</button>
                    </li>
                    <li className="text-right text-success title">
                    เฟรนฟาย x 1 = 140
                    <button className="btn btn-light btn-sm">X</button>
                    </li>
                </ul>
                <hr />
                <button className="btn bnk-block btn-danger title">ยืนยัน</button>
                <button className="btn bnk-block btn-secondary title">ยกเลิก</button>
            </div>
        )
    }
}

export default Calculator;